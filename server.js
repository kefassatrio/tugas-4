import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { MongoClient } from "mongodb";

const EXPRESS_PORT = 7878

const uri =
  "mongodb+srv://kefassatrio:9ze559AMMBsToTTd@cluster0.hwob7.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri);

const expressApp = express()
expressApp.use(cors())
expressApp.use(bodyParser.urlencoded({ extended: false }))
expressApp.use(bodyParser.json())

async function run() {
    try {
        await client.connect();
        const database = client.db('tugas4');
        const mahasiswaCollection = database.collection('mahasiswa');

        expressApp.get('/:npm', (req, res) => {
            const npm = req.params.npm;
            console.log(npm)
            const query = { npm: npm };
            (async () => {
                await mahasiswaCollection.findOne(query, (err, existingMahasiswa) => {
                    if(err) {
                        console.log(err)
                        return res.json({status: 'NOT OK'});
                    }
                    if(existingMahasiswa) {
                        return res.json({status: 'OK', npm: existingMahasiswa.npm, nama: existingMahasiswa.nama});
                    }
                    console.log(existingMahasiswa)
                    return res.json({status: 'NOT OK'});
                });
            })();
        })

        expressApp.listen(EXPRESS_PORT, () => console.log(`(express app) listening on port ${EXPRESS_PORT}!`))
    } finally {
        // await client.close();
    }
}

run().catch(console.dir);